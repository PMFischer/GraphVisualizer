const fs = require('fs');

const parseAssetAudit = (filePath) => {
    console.log(`File Path: ${filePath}`);

    let rawData = fs.readFileSync(filePath);
    let auditData = JSON.parse(rawData);
    let assets = auditData["Assets"];
    let assetNames = [];
    for(let asset of assets)
    {
        assetNames.push(Object.keys(asset)[0]);
    }

    return assetNames;
}

export {parseAssetAudit};