import { app, dialog , window, ipcRenderer} from "electron";
const helper = require('../helpers/graphparse');
const appWindow = require('../main');

export default {
  label: "App",
  submenu: [
    {
      label: "Open Audit Data",
      accelerator: "CmdOrCtrl+O",
      click: (menuItem, browserWindow, event) => {
        const openPromise = dialog.showOpenDialog({ properties: ['openFile'] });
        openPromise.then((value) => {

          if(value === undefined)
          {
            console.log("no file selected");
            return;
          }

          if(value.filePaths.length <= 0)
          {
            console.log("no file selected");
            return;
          }

            var filepath = Object.values(value.filePaths)[0];
            console.log(typeof(filepath));
            if(filepath === undefined)
            {
              console.log("no file selected");
              return;
            }

            // Display Graph Data here
            var assetData = helper.parseAssetAudit(filepath);
            browserWindow.webContents.send("asset-list-updated", assetData);
            //console.log(assetData);
         });
        
      }
    },
    {
      label: "Quit",
      accelerator: "CmdOrCtrl+Q",
      click: () => {
        app.quit();
      }
    }
  ]
};
