/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/hello_world/hello_world.js":
/*!****************************************!*\
  !*** ./src/hello_world/hello_world.js ***!
  \****************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "bye": () => (/* binding */ bye),
/* harmony export */   "greet": () => (/* binding */ greet),
/* harmony export */   "unrealgraph": () => (/* binding */ unrealgraph)
/* harmony export */ });
const greet = () => {
  return "Hello World!";
};
const unrealgraph = () => {
  return "Unreal Graph";
};
const bye = () => {
  return "See ya!";
};

/***/ }),

/***/ "./src/hello_world/hello_world.spec.js":
/*!*********************************************!*\
  !*** ./src/hello_world/hello_world.spec.js ***!
  \*********************************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var chai__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! chai */ "chai");
/* harmony import */ var chai__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(chai__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _hello_world__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./hello_world */ "./src/hello_world/hello_world.js");
/* harmony import */ var env__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! env */ "./config/env_test.json");



describe("hello world", () => {
  it("greets", () => {
    (0,chai__WEBPACK_IMPORTED_MODULE_0__.expect)((0,_hello_world__WEBPACK_IMPORTED_MODULE_1__.greet)()).to.equal("Hello World!");
  });
  it("says goodbye", () => {
    (0,chai__WEBPACK_IMPORTED_MODULE_0__.expect)((0,_hello_world__WEBPACK_IMPORTED_MODULE_1__.bye)()).to.equal("See ya!");
  });
  it("should load test environment variables", () => {
    (0,chai__WEBPACK_IMPORTED_MODULE_0__.expect)(env__WEBPACK_IMPORTED_MODULE_2__.name).to.equal("test");
    (0,chai__WEBPACK_IMPORTED_MODULE_0__.expect)(env__WEBPACK_IMPORTED_MODULE_2__.description).to.equal("Add here any environment specific stuff you like.");
  });
});

/***/ }),

/***/ "chai":
/*!***********************!*\
  !*** external "chai" ***!
  \***********************/
/***/ ((module) => {

module.exports = require("chai");

/***/ }),

/***/ "./config/env_test.json":
/*!******************************!*\
  !*** ./config/env_test.json ***!
  \******************************/
/***/ ((module) => {

module.exports = JSON.parse('{"name":"test","description":"Add here any environment specific stuff you like."}');

/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/compat get default export */
/******/ 	(() => {
/******/ 		// getDefaultExport function for compatibility with non-harmony modules
/******/ 		__webpack_require__.n = (module) => {
/******/ 			var getter = module && module.__esModule ?
/******/ 				() => (module['default']) :
/******/ 				() => (module);
/******/ 			__webpack_require__.d(getter, { a: getter });
/******/ 			return getter;
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/define property getters */
/******/ 	(() => {
/******/ 		// define getter functions for harmony exports
/******/ 		__webpack_require__.d = (exports, definition) => {
/******/ 			for(var key in definition) {
/******/ 				if(__webpack_require__.o(definition, key) && !__webpack_require__.o(exports, key)) {
/******/ 					Object.defineProperty(exports, key, { enumerable: true, get: definition[key] });
/******/ 				}
/******/ 			}
/******/ 		};
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/hasOwnProperty shorthand */
/******/ 	(() => {
/******/ 		__webpack_require__.o = (obj, prop) => (Object.prototype.hasOwnProperty.call(obj, prop))
/******/ 	})();
/******/ 	
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!*****************************!*\
  !*** ./temp/specs_entry.js ***!
  \*****************************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _src_hello_world_hello_world_spec_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../src/hello_world/hello_world.spec.js */ "./src/hello_world/hello_world.spec.js");

})();

/******/ })()
;
//# sourceMappingURL=specs.js.map